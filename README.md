# Samsung Software Membership Creative Project #
## 과제 배경 ##
 *  파노라마 이미지를 이용한 여러가지 컨텐츠들이 나왔지만 이를 이용한 컨텐츠는 기술에 비해 미약하다고 할 수 있다. 그 대표적인 이유중 하나는 파노라마 이미지를 만드는 연구는 활발히 이뤄지고 있지만 파노라마 이미지를 이용한 영상처리의 연구가 미약하기 때문이다. 우리는 이에 초점을 두어 파노라마 이미지를 이용해 특정부분 이미지와 유사도 분석을 하고 파노라마 이미지 연구에 대한 방향성을 제시함이 목적이다. 

## 사용 기술 ##
 * CUDA, opencv , Mysql, jsp, apache, Visual Studio 2010

## 구현 내용 ##
### 본 프로젝트는 다수의 파노라마 이미지와 그에 속하는 파노라마 이미지의 부분적 이미지의 유사도 매칭을 통해 해당하는 파노라마 사진을 유추해 결과를 찾아낸다. 이 때 현재 강력하게 이미지 분류에 쓰이고 있는 SVM을 적용시켜 유사도 인식에 따른 결과를 도출한다. ###

### 유사도 분석을 하기 전 빅데이터 셋을 만들기 위해 다음과 같은 프로세스를 따를 것이다. ###
    1. 테스트를 하기위한 로드뷰 영역 설정(예 : 서울시 종로구 원남동)
    2. 영역의 위도 경도를 바탕으로한 다음 로드뷰 서버 파노라마 데이터 요청
    3. grab-cut을 통한 이미지 전처리 작업
    4. 전처리 작업을 거친 이미지를 보정하여 필요한 데이터 셋으로 변환
    5. 변환후 Database 저장

### 파노라마 이미지 라벨링을 하기위한 프로세스 ###
    1. 학습에 필요한 이미지 데이터 베이스 구축 (예 : 건물, 차, 나무, 도로 ... 등)
    2. 학습에 필요한 알고리즘 사용 
         - Feature Extraction (SIFT)
         - Clustering (k-means clustering)
         - Codebook Generation
    3. 파노라마 이미지와 Codebook의 히스토그램 분석을통한 라벨링
        - Image Representation (histogram of feature)
        - Learning(SVM) and Recognition(histogram intersection)
        - ESS(Efficient Subwindow Search) 적용 (SVM을 보완한 작업)
        - Scene Labeling
    4. 분석된 파노라마 이미지 라벨링 데이터 DB화
    5. 비교 사진에 대한 이미지 코드북화
    6. 코드북화된 특징벡터와 파노라마 이미지 매칭
    7. 가장 유사도가 높은 파노라마 이미지 출력
### 본 프로젝트에 대한 결과는 다음과 같다. ###
    1. Window App을 이용하여 결과를 보여줄 것이다.
        - 다음 로드뷰의 사진의 부분적 매칭을 통한 결과
        - Random한 환경의 사진의 부분적 매칭을 통한 결과유사도가 높은 파노라마 이미지 출력

### Window App 예시 사진 ###
![캡처.PNG](https://bitbucket.org/repo/x6qERb/images/4021596936-%EC%BA%A1%EC%B2%98.PNG)
      