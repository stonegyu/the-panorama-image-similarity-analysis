/*M///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//
//                        Intel License Agreement
//                For Open Source Computer Vision Library
//
// Copyright (C) 2000, Intel Corporation, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//   * Redistribution's of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//   * Redistribution's in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//
//   * The name of Intel Corporation may not be used to endorse or promote products
//     derived from this software without specific prior written permission.
//
// This software is provided by the copyright holders and contributors "as is" and
// any express or implied warranties, including, but not limited to, the implied
// warranties of merchantability and fitness for a particular purpose are disclaimed.
// In no event shall the Intel Corporation or contributors be liable for any direct,
// indirect, incidental, special, exemplary, or consequential damages
// (including, but not limited to, procurement of substitute goods or services;
// loss of use, data, or profits; or business interruption) however caused
// and on any theory of liability, whether in contract, strict liability,
// or tort (including negligence or otherwise) arising in any way out of
// the use of this software, even if advised of the possibility of such damage.
//
//M*/

#include "creative_project.h"

#define BUILD	5				// SELECT BUILD (1 : CodeBook Generation, 2 : CodeBook's Histogram extract, 3 : SVM training, 4 : Image Labeling, 5 : Euclidian matching)

int main()
{

#if BUILD == 1

	first_step();				// Step 1 - CodeBook Generation

#elif BUILD == 2

	second_step();				// Step 2 - CodeBook's Histogram extract (SVM training Data-Set extract)

#elif BUILD == 3

	third_step();				// step 3 - SVM training

#elif BUILD == 4

	fourth_step();				// step 4 - Image Labeling

#elif BUILD == 5

	fifth_step();				// step 5 - Euclidian matching

#endif

	cout << endl << "step " << BUILD << " is done !!!" << endl;

	return 0;
}