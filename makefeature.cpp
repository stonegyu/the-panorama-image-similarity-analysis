#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "opencv2/nonfree/nonfree.hpp"

using namespace cv;

#define THRESHOLD 2.7

int main()
{
	initModule_nonfree();

	char * filename = new char[200];
	char * filename1 = new char[200];
	char * filename2 = new char[200];

	for(int i = 1; i < 178; i++)
	{
		if(i < 10)
			sprintf(filename,"C:\\testimages\\big_distortion\\pano00%d.jpg",i);
		else if(i < 100)
			sprintf(filename,"C:\\testimages\\big_distortion\\pano0%d.jpg",i);
		else
			sprintf(filename,"C:\\testimages\\big_distortion\\pano%d.jpg",i);

		IplImage* surf_img = cvLoadImage(filename, CV_LOAD_IMAGE_GRAYSCALE);

		CvSeq* temp_keypoints;
		CvSeq* temp_descriptors;
		CvMemStorage* storage = cvCreateMemStorage(0);
		CvSURFParams params = cvSURFParams(12000, 1);

		cvExtractSURF(surf_img, 0, &temp_keypoints, &temp_descriptors, storage, params);


		char* read_binary_panorama_name = new char[200];
		sprintf(read_binary_panorama_name, "C:\\testimages\\binary_256_80000\\result_binary_256_%d.jpg",i);
		Mat binary_pano = imread(read_binary_panorama_name,0);

		for(int ii = 0; ii < temp_keypoints->total; ii++)
		{
			CvSURFPoint* pt = (CvSURFPoint*)cvGetSeqElem(temp_keypoints, ii);
			if(binary_pano.at<uchar>(pt->pt.y, pt->pt.x) > 100)
			{
				//cvSeqRemoveSlice(temp_keypoints, cvSlice(ii,ii));
				cvSeqRemove(temp_keypoints, ii);
				cvSeqRemove(temp_descriptors, ii);
			}
		}

		sprintf(filename1,"C:\\testimages\\surf_keypoints&descriptors_256_12000\\pano_%d_keypoints.yml",i);
		sprintf(filename2,"C:\\testimages\\surf_keypoints&descriptors_256_12000\\pano_%d_descriptors.yml",i);

		// WRITE
		cvSave(filename1,temp_keypoints);
		cvSave(filename2,temp_descriptors);

		cvReleaseImage(&surf_img);
		cvClearSeq(temp_keypoints);
		cvClearSeq(temp_descriptors);
		cvReleaseMemStorage(&storage);
		printf("%d / 177 ing...\n",i);
	}
	printf("done !!!\n");
	return 0;
}