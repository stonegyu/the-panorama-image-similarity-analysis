// step 1. CodeBook Generation

#include "creative_project.h"

void first_step()
{
	char * filename = new char[100];
	Mat input;
	vector<KeyPoint> keypoints;
	Mat descriptor;
	Mat featuresUnclustered;
	SiftDescriptorExtractor detector;

	char dataset[2][20]={"building","car"};
	printf("NOW IS MAKING CODEWORD(KEY POINT DETECT)");
	for(int j=0; j<CATEGORY_SIZE; j++)
	{
		for(int f = CODEWORD_START; f <= CODEWORD_END; f++)
		{
			if(f < 10)
				sprintf(filename,"C:\\testimages\\result\\panorama_test_02\\test_03\\%s_03\\%s_00%d.jpg",dataset[j],dataset[j],f);
			else
				sprintf(filename,"C:\\testimages\\result\\panorama_test_02\\test_03\\%s_03\\%s_0%d.jpg",dataset[j],dataset[j],f);

			input = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
			detector.detect(input, keypoints);

			if(keypoints.size() < 200)
				printf("Not enough %s_%d  :  %d\n",dataset[j],f,keypoints.size());
			detector.compute(input, keypoints, descriptor);

			featuresUnclustered.push_back(descriptor);
			printf("��");
		}
	}
	printf("NOW IS DONE !!! (KEY POINT DETECT)\n");

	TermCriteria tc(CV_TERMCRIT_ITER, 100, 0.001);
	BOWKMeansTrainer bowTrainer(DICTIONARY_SIZE_K, tc, 3, KMEANS_PP_CENTERS);
	printf("NOW IS CLUSTERING (K MEANS CLUSTERING)");
	Mat dictionary = bowTrainer.cluster(featuresUnclustered);
	printf("NOW IS DONE !!! (K MEANS CLUSTERING)\n");
	/* CodeBook Storage */
	FileStorage fs("dictionary.yml", FileStorage::WRITE);
	fs << "vocabulary" << dictionary;
	fs.release();
}