// step 2. CodeBook's Histogram extract (SVM training Data-Set extract)

#include "creative_project.h"

void second_step()
{
	char * filename = new char[100];
	vector<KeyPoint> keypoints;

	FileStorage fs("dictionary.yml", FileStorage::READ);
	fs["vocabulary"] >> dictionary;
	fs.release();

	_bowDE.setVocabulary(dictionary);

	char dataset[2][20]={"building","car"};
	int count[2]={0,};
	printf("NOW IS MAKING HISTOGRAM(KEY POINT DETECT & COMPUTE)");
	for(int j=0; j<CATEGORY_SIZE; j++)
	{
		for(int f = CODEWORD_START; f <= CODEWORD_END; f++)
		{
			if(f < 10)
				sprintf(filename,"C:\\testimages\\result\\panorama_test_02\\test_03\\%s_03\\%s_00%d.jpg",dataset[j],dataset[j],f);
			else
				sprintf(filename,"C:\\testimages\\result\\panorama_test_02\\test_03\\%s_03\\%s_0%d.jpg",dataset[j],dataset[j],f);

			Mat code_img = imread(filename,CV_LOAD_IMAGE_GRAYSCALE);
			_detector->detect(code_img, keypoints);
			Mat codeword_bowDescriptor_histogram_all;
			_bowDE.compute(code_img, keypoints, codeword_bowDescriptor_histogram_all);

			codeword_bowDescriptor_histogram.push_back(codeword_bowDescriptor_histogram_all);
			printf("��");
		}
		count[j] = codeword_bowDescriptor_histogram.rows;
	}
	printf("NOW IS DONE !!! (KEY POINT DETECT & COMPUTE)");

	FileStorage fs2("traning_dataset.yml", FileStorage::WRITE);
	fs2 << "vocabulary" << codeword_bowDescriptor_histogram;
	fs2.release();
}