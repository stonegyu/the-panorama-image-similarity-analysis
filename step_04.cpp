// step 4. Image Labeling

#include "creative_project.h"

void fourth_step()
{
	// svm 불러오기
	CvSVM loaded_svm;
	loaded_svm.load("svm.yml",0);
	// Dictionary	   불러오기
	FileStorage fs1("dictionary.yml", FileStorage::READ);
	fs1["vocabulary"] >> dictionary;
	fs1.release();

	_bowDE.setVocabulary(dictionary);			// ★ 설정

	char * filename = new char[100];

	Mat input_image_bowDescriptor;
	sprintf(filename,"C:\\testimages\\image\\panorama1\\pano_002.jpg");   // 4096 * 2048
	Mat Full_Panorama_img = imread(filename, 1);

	// SURF 특징점 추출 후 저장
	CvSeq* temp_keypoints; CvSeq* temp_descriptors;
	CvMemStorage* storage = cvCreateMemStorage(0);
	CvSURFParams params = cvSURFParams(6500, 1);
	IplImage* surf_img = cvLoadImage(filename, CV_LOAD_IMAGE_GRAYSCALE);
	cvExtractSURF(surf_img, 0, &temp_keypoints, &temp_descriptors, storage, params);	// surf 특징점 추출
	
	// WRITE
	cvSave("pano1_keypoints.yml",temp_keypoints);
	cvSave("pano1_descriptors.yml",temp_descriptors);


	int width = 32, height = 16;			// 이미지 분할 갯수 정하기 16 x 16 / 10 x 10 / ...
	Mat sub_panorama[512];					// 파노라마 이미지 분할 개수에 맞춰서 배열선언
	int exist_keypoint_addr[512];			// 파노라마 이미지 분할 개수에 맞춰서 배열선언
	int subpana_arr_Number = 0;				// 분할되는 파노라마의 index number

	/*이미지 분할 및 분할한 이미지에서 특징점 추출*/
	for(int i = 0; i < height; i++)
	{
		for(int j = 0; j < width; j++)
		{
			Rect rect(j*(4096/width), i*(2048/height), (4096/width), (2048/height));
			sub_panorama[subpana_arr_Number] = Full_Panorama_img(rect);					// 이미지 분할 작업
			vector<KeyPoint> input_image_keypoints;
			_detector->detect(sub_panorama[subpana_arr_Number], input_image_keypoints);

			Mat input_image_bowDescriptor_all;
			_bowDE.compute(sub_panorama[subpana_arr_Number], input_image_keypoints, input_image_bowDescriptor_all);
			if(input_image_bowDescriptor_all.rows == 0)
				exist_keypoint_addr[subpana_arr_Number] = 0;
			else
				exist_keypoint_addr[subpana_arr_Number] = 1;
			subpana_arr_Number++;
			input_image_bowDescriptor.push_back(input_image_bowDescriptor_all);
		}
		printf("%d\n",i);
	}

	// SVM 분류
	Mat predicted = Mat(input_image_bowDescriptor.rows, 1, CV_32F);
	for(int i = 0; i < input_image_bowDescriptor.rows; i++)
	{
		Mat sample = input_image_bowDescriptor.row(i);
		predicted.at<float>(i,0) = loaded_svm.predict(sample);
	}

	int outImageWidth = 4096;
	int outImageHeight = 2048;

	IplImage* dst;
	IplImage *src;
	dst = cvCreateImage(cvSize(outImageWidth, outImageHeight), 8, 3);

	int subimg_cnt=0,predict_cnt=0;
	/*image labeling*/
	for(int i = 0; i < height; i++)
	{
		for(int j = 0; j < width; j++)
		{
			IplImage *input_image = &IplImage(sub_panorama[i*width+j]);
			Mat input = cvarrToMat(input_image);
			Mat blue = Mat(input.rows, input.cols, CV_8UC3, Scalar(255,0,0));
			Mat green = Mat(input.rows, input.cols, CV_8UC3, Scalar(0,255,0));
			Mat red = Mat(input.rows, input.cols, CV_8UC3, Scalar(0,0,255));
			Mat white = Mat(input.rows, input.cols, CV_8UC3, Scalar(255,255,255));
			double alpha = 0.5;
			double beta = 1.0 - alpha;
			if(exist_keypoint_addr[subimg_cnt] == 1)
			{
				if(predicted.at<float>(predict_cnt) == 1)
					addWeighted(input, alpha, green, beta, 0.0, input);
				else if(predicted.at<float>(predict_cnt) == 2)
					addWeighted(input, alpha, blue, beta, 0.0, input);
				predict_cnt++;
			}
			else
				addWeighted(input, alpha, blue, beta, 0.0, input);

			src = input_image;
			cvSetImageROI(dst, cvRect((src->width)*j, (src->height)*i, src->width, src->height));
			cvCopy(src,dst);

			subimg_cnt++;
		}
	}

	Mat output_image;

	cvResetImageROI(dst);
	output_image = cvarrToMat(dst);

	char filename2[100];
	sprintf(filename2,"C:\\testimages\\result\\panorama_test03\\pano2_sub_%d_gamma %d_c %d.jpg",width*height,(int)GAMMA,(int)C);
	imwrite(filename2, output_image);

	cvReleaseImage(&dst);
}