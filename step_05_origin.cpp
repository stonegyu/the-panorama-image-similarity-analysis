// step 5. Euclidian matching

#include "creative_project.h"
#include <fstream>
#include <windows.h>

//int eu_count = 0;

void swap(int &a, int &b)
{
	int temp; temp = a; a = b; b = temp;
}

void bubble_sort(int _vec[], int ind_vec[], int n)
{
	for(int i = 0; i < n-1; i++)
		for(int j = 0; j < n-1; j++)
			if(_vec[j] < _vec[j+1])
			{
				swap(_vec[j], _vec[j+1]);
				swap(ind_vec[j], ind_vec[j+1]);
			}
}

double euclidDistance(float* vec1, float* vec2, int length)
{
	double sum = 0.0;
	for (int i = 0; i < length; i++)
	{
		//eu_count++;
		sum += (vec1[i] - vec2[i]) * (vec1[i] - vec2[i]);
	}
	return sqrt(sum);
}

int nearestNeighbor(float* vec, int laplacian, CvSeq* keypoints, CvSeq* descriptors)
{
	int neighbor = -1;
	double minDist = 1e6;

	for (int i = 0; i < descriptors->total; i++)
	{
		CvSURFPoint* pt = (CvSURFPoint*)cvGetSeqElem(keypoints, i);
		// 라플라시안이 다른 키포인트는 무시
		if (laplacian != pt->laplacian)
			continue;
		float* v = (float*)cvGetSeqElem(descriptors, i);
		double d = euclidDistance(vec, v, DIM_VECTOR);
		// 보다 가까운 점이 있으면 옮겨 놓기
		if (d < minDist)
		{
			minDist = d;
			neighbor = i;
		}
	}
	// 최근접점에서도 거리가 임계값 이상이라면 무시한다.
	if (minDist < THRESHOLD)
		return neighbor;
	// 최근접점이 없는 경우
	return -1;
}
//int abcde = 0;
void findPairs(CvSeq* keypoints1, CvSeq* descriptors1, CvSeq* keypoints2, CvSeq* descriptors2, vector<int>& ptpairs/*, Mat* pano_binary*/)
{
	ptpairs.clear();
	// 영상 1의 각 키포인트에 관해서 최근접점을 검색
	for (int i = 0; i < descriptors1->total; i++)
	{
		CvSURFPoint* pt1 = (CvSURFPoint*)cvGetSeqElem(keypoints1, i);

		//printf("%d번째 특징점 좌표 : %f , %f\n", i, pt1->pt.x , pt1->pt.y);
		//printf("해당 특징점 값 : %d\n",pano_binary->at<uchar>(pt1->pt.y, pt1->pt.x));
		//if(pano_binary->at<uchar>(pt1->pt.y, pt1->pt.x) > 100)
		//	continue;

		float* desc1 = (float*)cvGetSeqElem(descriptors1, i);
		// 최근접점의 검색
		int nn = nearestNeighbor(desc1, pt1->laplacian, keypoints2, descriptors2);
		// 최근접점이 있을 경우 영상 1의 인덱스와 영상 2의 인덱스를 차례로 등록
		if (nn >= 0)
		{
			ptpairs.push_back(i);
			ptpairs.push_back(nn);
		}
	}
}

void fifth_step()
{
	initModule_nonfree();
	//int ind = 15;

	for(int k = 1; k < 2; k++)
	{
		// 시간측정
		LARGE_INTEGER liCounter1, liCounter2, liFrequency;
		QueryPerformanceFrequency(&liFrequency);
		QueryPerformanceCounter(&liCounter1);   // start

		int final_array[178] = {0,};
		int final_ind_array[178] = {0,};

		char* input_imagename = new char[200];
		sprintf(input_imagename,"C:\\testimages\\input_images\\%d.jpg",k);

		// 이미지 로드
		IplImage* loaded_Image = cvLoadImage(input_imagename, CV_LOAD_IMAGE_GRAYSCALE);
		CvSeq *keypoints2 = 0, *descriptors2 = 0;		// 인풋 이미지에서 추출한 것
		// SURF의 추출(1회)
		cvExtractSURF(loaded_Image, 0, &keypoints2, &descriptors2, cvCreateMemStorage(0), cvSURFParams(12000, 1));

		char* read_keypoints_name = new char[200];
		char* read_descriptors_name = new char[200];
		char* read_binary_panorama_name = new char[200];

		for(int i = 1; i < 178; i++)
		{
			sprintf(read_keypoints_name,"C:\\testimages\\surf_keypoints&descriptors12000\\pano_%d_keypoints.yml",i);
			sprintf(read_descriptors_name, "C:\\testimages\\surf_keypoints&descriptors12000\\pano_%d_descriptors.yml",i);

			CvSeq *keypoints1 = 0, *descriptors1 = 0;		// 읽어올 것

			// READ
			CvMemStorage* storage = cvCreateMemStorage(0);
			keypoints1 = (CvSeq*)cvLoad(read_keypoints_name,storage);
			descriptors1 = (CvSeq*)cvLoad(read_descriptors_name,storage);		// 여기서 메모리 증가

			// read binary panorama
			//sprintf(read_binary_panorama_name, "C:\\testimages\\binary_80000\\result_binary_%d.jpg",i);
			//Mat binary_pano = imread(read_binary_panorama_name,0);

			// 유사도 판별
			vector<int> ptpairs;
			//findPairs(keypoints1, descriptors1, keypoints2, descriptors2, ptpairs, &binary_pano);
			findPairs(keypoints1, descriptors1, keypoints2, descriptors2, ptpairs);

			// 순위 판별
			final_array[i] = ptpairs.size();
			final_ind_array[i] = i;

			// 후처리 - 메모리 해제 등
			cvClearSeq(keypoints1);
			cvClearSeq(descriptors1);
			cvClearMemStorage(storage);
			cvReleaseMemStorage(&storage);
			ptpairs.clear();
			printf("%d / 177 ing...\n",i);
		}

		cvReleaseImage(&loaded_Image);
		cvClearSeq(keypoints2);
		cvClearSeq(descriptors2);

		bubble_sort(final_array,final_ind_array,177);

		//for(int i = 0; i <= 10; i++)
		printf("%d번째 사진과 유사한 사진 %d순위 : %d번 파노라마\n", k, 1, final_ind_array[0]);

		QueryPerformanceCounter(&liCounter2);   // end
		//printf("Time : %f\n", (double)(liCounter2.QuadPart - liCounter1.QuadPart) / (double)liFrequency.QuadPart);
		//printf("eu_count : %d\n", eu_count);
	}
}